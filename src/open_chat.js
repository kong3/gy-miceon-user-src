import React, { Component } from 'react';

class OpenChat extends Component {
    render() {
        let windowWidth = $(window).width() + 'px';
        let size = window.matchMedia('(max-width: 768px)');
        if(size) 
        {
            document.body.style.width=windowWidth;
            document.getElementById('grid_footer').style.width = windowWidth;
            document.getElementById('grid_wrapper').style.width = windowWidth;
        };
    };
};
export default OpenChat;