import React, {Fragment} from 'react';

function NavSectionMenu() {
    return (
        <>
        <div className="wrapper" id="grid_wrapper">
            <div><a href="./"><img src="./src/img/event.png" alt="" style={{width:"auto",height:"auto"}} /></a><p>행사일정</p></div>
            <div><a href="./"><img src="./src/img/rest.png" alt="" style={{width:"auto",height:"auto"}} /></a><p>식당&카페</p></div>
            <div><a href="./"><img src="./src/img/hotel.png" alt="" style={{width:"auto",height:"auto"}} /></a><p>숙박</p></div>
            <div><a href="./"><img src="./src/img/shop.png" alt="" style={{width:"auto",height:"auto"}} /></a><p>쇼핑</p></div>
            <div><a href="./"><img src="./src/img/tour.png" alt="" style={{width:"auto",height:"auto"}} /></a><p>관광</p></div>
            <div><a href="./"><img src="./src/img/emer.png" alt="" style={{width:"auto",height:"auto"}} /></a><p>응급</p></div>
        </div>
        <br />
        <br />
        <hr />
        </>
    );
}
  
export default NavSectionMenu;
  