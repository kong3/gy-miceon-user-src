import React, {Fragment} from 'react';

function Footer() {
    return (
        <>
         <footer className="footer" id="grid_footer">
            <div><a href="./"><img src="./src/img/home.png" alt="" style={{width:"auto",height:"auto" }} /></a><p>홈</p></div>
            <div><a href="./"><img src="./src/img/heart.png" alt="" style={{width:"auto",height:"auto" }} /></a><p>찜리스트</p></div>
            <div><a href="./"><img src="./src/img/calendar.png" alt="" style={{width:"auto",height:"auto" }} /></a><p>예약내역</p></div>
            <div><a href="./"><img src="./src/img/star.png" alt="" style={{width:"auto",height:"auto" }} /></a><p>나의평가</p></div>
            <div><a href="./"><img src="./src/img/dots.png" alt="" style={{width:"auto",height:"auto" }} /></a><p>더보기</p></div>
        </footer>
        </>
    );
}
  
export default Footer;
  