import React, {Fragment} from 'react';

function Header() {
    return (
        <>
        <header className="main_wrap_header" id="main_wrap_header">
            <a className="goyang" href="./" id="goyang">고양 <strong>MICE</strong> ON</a>
            <a className="searching" id="searching" href="./"><img id="search_icon" className="search_icon" src="./src/img/iconSearch@2x.png" alt="" /></a>
            <input id="input_place" type="text" placeholder="일산 가로수길에 뜨고 있는 그곳 어디?" className="input_place"></input>
        </header>
        </>
    );
}
  
export default Header;
  