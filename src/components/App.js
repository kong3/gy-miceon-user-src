import React, { Fragment } from 'react';
import Header from './Header';
import NavSection from './NavSection';
import NavSectionMenu from './NavSectionMenu';
import Footer from './Footer';
import './home.css';

const App = () => {
  return (
    <Fragment>
      <Header />
      <NavSection />
      <NavSectionMenu />
      <Footer />
    </Fragment>
  );
};

export default App;
