### Develop && Production Project 구조
```
gy-miceon
    ㄴ gy-miceon-user
    ㄴ gy-miceon-enter
    ㄴ gy-miceon-admin
    ㄴ dist
        ㄴ user
        ㄴ enter
        ㄴ admin
    ㄴ gy-miceon-back
```

### 간단설명
- #### Front
 1. Front에서 git pull을 받는다.
 2. 각 폴더에서 yarn build를 하면 상위 폴더에 dist폴더가 생성되며 webpack bundle로 만든 파일들이 생긴다.
 3. bundle로 만든 파일들은 nginx container의 volume으로 들어간다.

### 기본세팅파일 (docker, nginx)
 - #### nginx
> mkdir nginx  
> cd nginx  
> vi default.conf  
```
server {

  listen 80; 

  location / { 
    root   /usr/share/nginx/html;
    index  index.html index.htm;
    try_files $uri $uri/ /index.html;
  }

  error_page   500 502 503 504  /50x.html;

  location = /50x.html {
    root   /usr/share/nginx/html;
  }
}
```

 - ### docker-compose
> vi docker-compose.yml
```
version: "3" 
services:
  nginx:
    container_name: proxyserver
    image: nginx:alpine
    ports:
      - '81:80'
    volumes:
      - ./dist:/usr/share/nginx/html
      - ./nginx/default.conf:/etc/nginx/conf.d/default.conf
    networks:
      - backend

  server:
    build:
      dockerfile: Dockerfile
      context: ./gy_miceon_backend
    volumes:
      - ./gy_miceon_backend:/app
      - /app/node_modules
    environment:
      - NODE_PATH=src
      - PORT=5000
      - DB_HOST=mongo
      - DB=test
    networks:
      - backend
    depends_on:
      - mongo
    ports:
      - "5000:5000"

  mongo:
    container_name: mongo
    image: mongo
    volumes:
      - ./mongodb/data:/data/db
      - ./mongodb/log:/var/log/mongodb
    ports:
      - "27017:27017"
    networks:
      - backend

networks:
  backend:
    driver: bridge
```


### Dependency 설치
```
yarn add react react-dom
yarn add -D webpack webpack-cli webpack-dev-server html-webpack-plugin mini-css-extract-plugin
yarn add -D @babel/core @babel/preset-env @babel/preset-react babel-loader css-loader file-loader style-loader url-loader
yarn add axios moment react-moment react-redux redux redux-devtools-extension redux-thunk uuid @material-ui/core
```

### Webpack 설정 (webpack.config.js)
```
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

module.exports = {
  entry: './src/index.js',
  output: {
    path: path.join(__dirname, '/dist/user'),
    filename: 'index_bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader'
        }
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: MiniCssExtractPlugin.loader,
            options: {
              publicPath: '../'
            }
          },
          'css-loader'
        ]
      },
      {
        test: /\.(png|jp(e*)g|svg)$/,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 8000, // Convert images < 8kb to base64 strings
              name: 'images/[hash]-[name].[ext]'
            }
          }
        ]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: './src/index.html'
    }),
    new MiniCssExtractPlugin({
      filename: 'bundle.css'
    })
  ]
};
```

### Babel preset 설정 (.babelrc)
```
{
  "presets": ["@babel/preset-env", "@babel/preset-react"]
}
```

### Eslint standard ruels
```
https://standardjs.com/rules.html
```
